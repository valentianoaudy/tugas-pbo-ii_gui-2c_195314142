package PBO3C;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JTextArea;
import javax.swing.JTextField;
public class Latihan4 extends JDialog {
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 180;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    public static void main(String[] args){
        Latihan4 dialog =  new Latihan4();
        dialog.setVisible(true);
    }   
    public Latihan4(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(true);
        setTitle("CheckBoxDemo");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    
        JTextArea text = new JTextArea("Welcome to_java");
        text.setBounds(190, 40, 150, 50);
        contentPane.add(text);

        JCheckBox box = new JCheckBox("Centered");
        box.setBounds(400, 5, 100, 21);
        contentPane.add(box);
        
        JCheckBox box1 = new JCheckBox("Bold");
        box1.setBounds(400, 40, 100, 21);
        contentPane.add(box1);
        
        JCheckBox box2 = new JCheckBox("Italic");
        box2.setBounds(400, 75, 150, 21);
        contentPane.add(box2);
        
        okButton = new JButton("Left");
        okButton.setBounds(160, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton("Right");
        cancelButton.setBounds(260, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
     }   
} 

