package PBO3C;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
public class Latihan5 extends JDialog {
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 190;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField txtField;
    public static void main(String[] args){
        Latihan5 dialog =  new Latihan5();
        dialog.setVisible(true);
    }   
    public Latihan5(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(true);
        setTitle("RadioButtonDemo");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    
        JTextArea text = new JTextArea("Welcome to_java");
        text.setBounds(175, 40, 150, 50);
        contentPane.add(text);

        JCheckBox box = new JCheckBox("Centered");
        box.setBounds(400, 5, 100, 21);
        contentPane.add(box);
        
        JCheckBox box1 = new JCheckBox("Bold");
        box1.setBounds(400, 40, 100, 21);
        contentPane.add(box1);
        
        JCheckBox box2 = new JCheckBox("Italic");
        box2.setBounds(400, 75, 100, 21);
        contentPane.add(box2);
        
        JRadioButton radio = new JRadioButton("Red");
        radio.setBounds(2, 5, 100, 21);
        contentPane.add(radio);
        
        JRadioButton radio2 = new JRadioButton("Green");
        radio2.setBounds(2, 40, 100, 21);
        contentPane.add(radio2);
        
        JRadioButton radio3 = new JRadioButton("Blue");
        radio3.setBounds(2, 75, 100, 21);
        contentPane.add(radio3);
        
        okButton = new JButton("Left");
        okButton.setBounds(160, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton("Right");
        cancelButton.setBounds(260, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
     }   
} 

