package PBO3C;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
public class Latihan6 extends JDialog {
private final JComboBox box;
private final JLabel image,text;
private final JList list;
private final JPanel listPanel;
private static final int FRAME_WIDTH = 500;
private static final int FRAME_HEIGHT = 250;
private static final int BUTTON_WIDTH = 484;
private static final int BUTTON_HEIGHT = 30;
public Latihan6 () {
String[] names = {"Indonesia",
"dengan nama asli Negara Kesatuan Republik Indonesia "
, " ",
"adalah negara di Asia Tenggara yang dilintasi garis khatulistiwa dan berada di antara daratan benua Asia dan Australia",
"serta antara Samudra Pasifik dan Samudra Hindia. Indonesia adalah negara kepulauan terbesar di dunia yang terdiri dari 17.504 pulau.",
"Nama alternatif yang biasa dipakai adalah Nusantara.[14] Dengan populasi Hampir 270.054.853 jiwa pada tahun 2018,",
"ndonesia adalah negara berpenduduk terbesar keempat di dunia dan negara yang berpenduduk Muslim terbesar di dunia, dengan lebih dari 230 juta jiwa.",
"Ibu kota negara Indonesia adalah Jakarta. Indonesia berbatasan darat dengan Malaysia di Pulau Kalimantan dan Pulau Sebatik,",
"dengan Papua Nugini di Pulau Papua dan dengan Timor Leste di Pulau Timor. Negara tetangga lainnya adalah Singapura, Filipina,",
"Australia, dan wilayah persatuan Kepulauan Andaman dan Nikobar di India."};
        Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(true);
        setTitle("ComboBoxDemo");
        contentPane.setLayout(null);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        ImageIcon img = new ImageIcon("bendera2.jpg");
        image = new JLabel();
        image.setIcon(img);
        image.setBounds(50,50,200,100);
        contentPane.add(image);
        box = new JComboBox();
        box.addItem("Indonesia");
        box.setBounds(1,1,BUTTON_WIDTH,BUTTON_HEIGHT);
        contentPane.add(box);
        text = new JLabel("Indonesia");
        text.setBounds(85, 100, 100, 150);
        contentPane.add(text);
        listPanel = new JPanel(new GridLayout(0, 1));
        list = new JList(names);
        listPanel.add(new JScrollPane(list));
        listPanel.setBounds(250,50,230,150);
        contentPane.add(listPanel);
       } 
        public static void main(String[] args) {
        Latihan6 dialog = new Latihan6 ();
        dialog.setVisible(true);
    }
}

