package PBO3C;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
public class Latihan3 extends JDialog {
    private JLabel image;
    private JLabel text;
    public Latihan3() {
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Text & Icon Label");
        this.setVisible(true);
        
        text= new JLabel("Shark");
        text.setBounds(115, 100, 99, 299);
        this.add(text); 

        ImageIcon img = new ImageIcon("hiu.png");
        image = new JLabel();
        image.setIcon(img);
        this.add(image); 
    }
    public static void main(String[] args) {
        new Latihan3();
    }
}
