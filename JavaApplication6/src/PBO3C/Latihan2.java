package PBO3C;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JDialog;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JMenuItem;
public class Latihan2 extends JDialog {
    private static final int FRAME_WIDTH = 350;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    private JButton yellowButton;
    private JButton blueButton;
    private JButton redButton;
    public static void main(String[] args){
        Latihan2 dialog =  new Latihan2();
        dialog.setVisible(true);
    }   
    public Latihan2(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(true);
        setTitle("ButtonTest");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);

        yellowButton = new JButton("Yellow");
        yellowButton.setBounds(30, 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(yellowButton);
        
        blueButton = new JButton("Blue");
        blueButton.setBounds(130, 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(blueButton);
        
        redButton = new JButton("Red");
        redButton.setBounds(230, 10, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(redButton);
     }   
} 