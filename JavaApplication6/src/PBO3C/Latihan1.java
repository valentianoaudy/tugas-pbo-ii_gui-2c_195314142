package PBO3C;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JMenuItem;
public class Latihan1 extends JFrame {
    private static final int FRAME_WIDTH = 450;
    private static final int FRAME_HEIGHT = 300;
    private static final int FRAME_X_ORIGIN = 300;
    private static final int FRAME_Y_ORIGIN = 100;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 40;
    public static void main(String[] args){
        Latihan1 frame =  new Latihan1();
        frame.setVisible(true);
    }   
    public Latihan1(){
        Container contentPane = getContentPane();
        setSize ( FRAME_WIDTH, FRAME_HEIGHT );
        setResizable(true);
        setTitle("                                              Frame Pertama");
        setLocation( FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        contentPane.setLayout(null);
        contentPane.setBackground(Color.PINK);

        JMenuItem menu = new JMenuItem("File");
        menu.setBounds(0, 0, 40, 21);
        contentPane.add(menu);
        menu.setBackground(Color.GRAY);
        
        JMenuItem menu1 = new JMenuItem("Edit");
        menu1.setBounds(35, 0, 40, 21);
        contentPane.add(menu1);
        menu1.setBackground(Color.GRAY);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
     }   
} 