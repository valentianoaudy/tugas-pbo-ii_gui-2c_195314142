package PBO3C;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
public class Latihan7 extends JDialog {
private final JList list;
private final JPanel listPanel; 
private JLabel image;
private static final int FRAME_WIDTH = 1000;
private static final int FRAME_HEIGHT = 500;
public Latihan7 () {
 String[] names = {"Kanada","Jerman","Indonesia","Malaysia","Jepang","Spanyol","Vietnam","Italy"
                   ,"Portugal","Kameron","Inggris"};
  Container contentPane = getContentPane();
        setSize(FRAME_WIDTH, FRAME_HEIGHT);
	setResizable(true);
	setTitle("ListDemo");
	contentPane.setLayout(null);
	setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);  
	ImageIcon img = new ImageIcon("bendera2.jpg");
	image = new JLabel();
	image.setIcon(img);
	image.setBounds(100,0,200,100);
	contentPane.add(image);
	ImageIcon i = new ImageIcon("bendera.jpg");
	image = new JLabel();
	image.setIcon(i);
	image.setBounds(250,0,200,150);
	contentPane.add(image);
	ImageIcon m = new ImageIcon("bendera1.jpg");
	image = new JLabel();
	image.setIcon(m);
	image.setBounds(450,0,500,250);
	contentPane.add(image);
	ImageIcon a = new ImageIcon("bendera3.jpg");
	image = new JLabel();
	image.setIcon(a);
	image.setBounds(100,100,300,250);
	contentPane.add(image);
	ImageIcon g = new ImageIcon("bendera4.jpg");
	image = new JLabel();
	image.setIcon(g);
	image.setBounds(450,250,250,200);
	contentPane.add(image);
	ImageIcon e = new ImageIcon("bendera5.jpg");
	image = new JLabel();
	image.setIcon(e);
	image.setBounds(750,250,250,200);
	contentPane.add(image);
	listPanel = new JPanel(new GridLayout(0, 1));
	list = new JList(names);
	listPanel.add(new JScrollPane(list));
	listPanel.setBounds(1,1,100,160);
	contentPane.add(listPanel);
}
  public static void main(String[] args) {
        Latihan7 dialog = new Latihan7 ();
        dialog.setVisible(true);
}
}